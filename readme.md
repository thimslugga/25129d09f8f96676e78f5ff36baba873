# PowerDNS

PowerDNS is not designed to provide recursive results. It is intended to act only as an authoritative server for the domains it serves. This implies it will be serving domain data to other hosts. 


# Install pdns and pdns-recursor

```     
sudo apt-get install pdns-server pdns-recursor
```

* edit recursor settings in `/etc/powerdns/recursor.conf`

```
local-port=53

forward-zones=mydomain.local=127.0.0.1:54 # change ‘mydomain.local’ to your domain that you’ll be hosting in PowerDNS 
```

* edit pdns settings in `/etc/powerdns/pdns.conf`

```
local-port=54

launch=gmysql
gmysql-host=127.0.0.1
gmysql-user=root
gmysql-dbname=pdns
gmysql-password=mysecretpassword
```


# MySQL

* install mysql server

* install module for pdns

```
sudo apt-get install pdns-backend-mysql
```

* add data to mysql

```



INSERT INTO domains (name, type) values ('my.local', 'NATIVE');

INSERT INTO records (domain_id, name, content, type,ttl,prio)
VALUES (1,'my.local','51.0.1.21','A',120,NULL);


INSERT INTO records (domain_id, name, content, type,ttl,prio)
VALUES (1,'my.local','localhost ahu@ds9a.nl 1','SOA',86400,NULL);

INSERT INTO records (domain_id, name, content, type,ttl,prio)
VALUES (1,'my.local','dns-us1.powerdns.net','NS',86400,NULL);

INSERT INTO records (domain_id, name, content, type,ttl,prio)
VALUES (1,'my.local','dns-eu1.powerdns.net','NS',86400,NULL);


INSERT INTO records (domain_id, name, content, type,ttl,prio)
VALUES (1,'mail.my.local','192.0.2.12','A',120,NULL);

INSERT INTO records (domain_id, name, content, type,ttl,prio)
VALUES (1,'my.local','mail.my.local','MX',120,25);

```
 

# Start

* start pdns and pdns-recursor
```
sudo /etc/init.d/pdns-recursor start
sudo /etc/init.d/pdns start
```

* check
```
nslookup linode.com 127.0.0.1

```
This should work, if it doesn’t, check /var/log/syslog for messages from the recursor


```
dig mydomain.local @127.0.0.1 -p 54
```

